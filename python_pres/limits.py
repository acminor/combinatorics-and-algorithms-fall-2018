import itertools
import time

def test_alg(alg):
    t = time.time()
    size = 1
    while time.time() - t < 30:
        alg(size)
        size+=1
        t = time.time()

def test_comb(size):
    temp = range(0,size)
    max_number = size/2
    return [i for i in itertools.combinations(temp, max_number)]

def test_permutation(size):
    pass

def test_comb_with_rep(size):
    pass

test_alg(test_comb)
