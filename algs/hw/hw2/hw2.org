#+TITLE: Homework 2
#+AUTHOR: Austin Minor (米诺)
#+DATE: \today
#+LATEX_HEADER: \usepackage{xeCJK}
#+LATEX_HEADER: \usepackage{algorithm2e}
#+LATEX_CLASS: article
#+LATEX_CLASS_OPTIONS: [twocolomn]

* Questions
1) 2.1-3
2) 2.2-3
3) 3.1-3
4) Show that $2n^2 + 3n + 1 = \Theta(n^2)$
* Answers
** Problem One
Below is one version of the algorithm for finding an element if it exists.
This algorithm finds the last occurrence of the element if it exists.

#+BEGIN_EXPORT latex
\begin{algorithm}[H]
 \KwData{A sequence of numbers $A=[a_1,a_2,...,a_n]$ and a value $v$.} 
 \KwResult{An index such that $A[i] = v$ or nil if $v$ is not in $A$.}
 $index = nil$\\
 \For{$i = 1$ to $n$}{
   \If{$A[i] = v$}{
     $index = i$
   }
 }
 \Return $index$
\end{algorithm}
#+END_EXPORT

To prove the correctness of this algorithm, we use a loop invariant.
This invariant is as follows:

#+BEGIN_EXAMPLE
Index is either nil or the value of the last index in A[1..i-1] that is equal to v.
#+END_EXAMPLE

To prove this correct, we must show that it is correct for initialization,
maintenance and termination.

+ Initialization

At the start index is set to $nil$, $i = 1$, and $A[1..i-1]$ is empty and thus the invariant holds.

+ Maintenance

We know from induction that index is either $nil$ or some $k \in [1..i-1]$ with $A[k] = v$.
Furthermore, it is the last such $k$ indexed element.

We consider the next element $A[i]$. If it is equal to v, then index becomes $i$
which is the last such $A[k] = v$ in $A[1..i]$. Otherwise, $A[k]$ remains either
$nil$ or the index $k$ which is now shown to be the last index in $A[1..i]$.

+ Termination

At termination, we have $i=n+1$. Plugging this into our loop invariant yields,
"index is either nil or the value of the last index in $A[1..n]$ that is equal to v".

Thus, we have our desired result.

** Problem Two
We assume that we use the quick return algorithm for linear search. Thus, we only check
either all the elements or the number of elements until we reach the first occurrence of the value.

We also (for the sake of simplicity) assume that the element being search for is not repeated.
Since all the elements in the array have the same probability, this also implies that all the
other elements are unique as well. Furthermore, we assume that the element does appear in the array.

We can now start to analyze the problem. Let, "X" be the random variable defined as "the index
in which A[i] = v". We can then take the expected value of this function to derive the
average index at which "A[i] = v". This expected value can be calculated as following
$E[X] = \sum_{i=1}^{n} i * Pr(A[i] = v$ and not found earlier in the list$)$.
Because of our initial assumptions, we know that
the probability is the same for each element. Furthermore, we know that the elements are unique.
Taking our original equation and furthering solving with these constraints in mind we find the following.
$E[X] = \sum_{i=1}^{n} i * Pr(A[i])Pr(A[1..i-1] != v| A[i] = v) =
\sum_{i=1}^{n} i * p*1$. This is again knowing the if the element only appears once, we are guaranteed
that the previous array elements are not equal to v. Furthermore, every array element has equal probability
which we can express with "p". This is the majority of the average case. Finally, we can use
Gauss's summation to reduce the final equation to the following; $E[X] = p\frac{n(n+1)}{2}$.
Furthermore, the element is guaranteed to appear in our array and all the array elements are unique.
Thus, the probability has to be $p = \frac{1}{n}$ (all the elements have the same probability and
the probability is not less than that of all the elements).

The worst case number of elements checked is all of the elements. This is clear and occurs
when the element does not exist in the array.

The average run time of the algorithm is determined as follows. We know the expected element
and the probability p. Substituting in we have the following: $E[X] = \frac{n+1}{2}$.
As this is a scaled linear term plus a constant. We know from the rules regarding polynomials
and Theta runtime growth that the runtime of this algorithm is $\Theta(n^2)$.

The worst-case run time is $\Theta(n)$. This is because the number of elements checked is always
n. Thus, the number of elements checked relies on n. This gives us a runtime of $f(n) = c_1*n + c_0$.
This function is linear and can be bound by $\Theta(n)$ like in the average case.

We are indebted to the book for reminding us of the various summation and probability rules and
for a long and fruitful discussion with the teacher and fellow classmates.

** Problem Two - Wrong (Geo can only apply to indep. events)       :noexport:
We assume that we use the quick return algorithm for linear search. Thus, we only check
either all the elements or the number of elements until we reach the first occurrence of the value.

With this, the average case becomes the min of the number of elements in the array of the expected
position that we find the first occurrence of the value. We can use random variables to find this
expected position.

Define the random variable "X" as $X = \sum_{i=1}^{n} i * Pr(A[i] = v$
is the first occurrence$)$. This probability is the probability that $A[i] = v$ and the
probability that none of the $A[1..i-1] = v$.

Thus, we have the sum $X = \sum_{i=1}^{n} i * p * (1-p)^{i-1} =
\frac{p}{1-p} * \sum_{i=1}^{n} i * (1-p)^i$ where p is an equal probability
that any element is the element being looked for. This last equality can be solved using
infinite sum tricks. Since, we are looking for the growth function at large scales,
we can assume that the sum is infinite and not finite. Thus we have
$X = \frac{p}{1-p} \frac{1-p}{p^2} = \frac{1}{p}$. The exact steps are omitted as they come
from the discussion on geometric distributions in the book.

Thus, the average elements checked is $min(\frac{1}{p}, n)$ where n is the number of elements
in the array.

The worst case number of elements checked is all of the elements. This is clear and occurs
when the element does not exist in the array.

The average run time of the algorithm is constant. This is due to the average element being constant
without respect to n. Thus for a really large n, the average elements checked does not depend on
n and is some constant defined by $\frac{1}{p}$. Thus, the function of runtime is $f(n) = c_0$.
Thus, it is constant time, $\Theta(1)$.

The worst-case run time is $\Theta(n)$. This is because the number of elements checked is always
n. Thus, the number of elements checked relies on n. This gives us a runtime of $f(n) = c_1*n = c_0$.
This function is linear and can be bound by $\Theta(n)$.

** Problem Three
The statement "The running time of algorithm A is at least O($n^2$)" is meaningless
because of the following reasons.
1) Big-Oh notation describes the upper bound of an algorithm.
2) At least implies a lower bound.

Thus, we have that the running time is $RunningTime \geq O(n^2)$
However, Big-Oh implies that $RunningTime \leq O(n^2)$. Thus,
the statement is meaningless.

** Problem Four
#+BEGIN_EXPORT latex
\[
2n^2 + 3n + 1 = \Theta(n^2)\\
<=>
c_0 n^2 \leq 2n^2 + 3n + 1 \geq c_1 n^2; n \geq n_0 \\
c_0 \leq 2n^2 + \frac{3}{n} + \frac{1}{n^2} \geq c_1\\
\]

The inner function has its max at $n=1$. Note, "n" cannot be zero and is an integer.
This max is $2 + 3 + 1 = 6$. The function is monatonically decreasing because as n increases
it divides from the positive numbers in the equation. Furthermore, we get the minimum as n goes
to infinity. This minimum is $2 + 0 + 0 = 2$.

If we choose $c_0 = 1$ and $c_1 = 7$, we have
$1 \leq 2n^2 + \frac{3}{n} + \frac{1}{n^2} \geq 7$
which we have shown for $n \geq 1$ to be true.

We are indebited to the books method of solving as we originally tried a more complex and fruitless method.
#+END_EXPORT
