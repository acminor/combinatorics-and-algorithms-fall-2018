#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct alg_args {
  int a;
  int b;
  int* arr1;
  int* arr2;
  int rng_low;
  int rng_high;
  int* count;
  int length;
} typedef alg_args;

int n2_merge(alg_args args) {
  int count = 0;
  int a_sum = 0;
  int i, j;
  for (i = (args.a + args.b)/2; i >= args.a; i--) {
    a_sum += args.arr1[i];
    int b_sum = 0;
    for (j = (args.a + args.b)/2 + 1; j < args.b; j++) {
      b_sum += args.arr1[j];
      /*
      if (args.rng_low <= (a_sum + b_sum) &&
                (a_sum + b_sum) <= args.rng_high) {
          printf("match at [%d, %d] with val {%d}\n", i, j, a_sum+b_sum);
      }
      */
      count += (args.rng_low <= (a_sum + b_sum) &&
              (a_sum + b_sum) <= args.rng_high);
    }
  }

  return count;
}

/*
  We know that this algorithm is truly N^2 running time. We were unable to discover the
  true NLogN algorithm. However, we were able to create some observations which are listed below.

  + If we have the original array and an array of the sums as below, we have
   - [a, b, c, d, ...]
   - [a, a+b, a+b+c, ...]
  + We can sort the sums array to have some array of the sums. With this
    we can perform the sliding window algorithm discussed in class with a couple of issues.
    1) We need to make sure we do not subtract a - a or a+b - a+b+c, etc.
  + With this, we could then have the following,
    - [z, x, y, ...] - [a+b+c], [z, x, y, ...] - [d+e+f] etc.
    - = [\z-a+b+c = d, y-+a+b+c = d+e\, ...] where the \\ denote sliding windows
    - Thus, it seems that if we could address (1) we would have a good protocol
      based on what the teacher said in class. Alas, we could not figure this out.
  + Thus, what is written below does not try to follow the above but implements
    a trivial divide and conquer algorithm based off the trivial iterative algorithm.
 */
int n2_alg(alg_args args) {
  if (args.a == args.b) {
    *(args.count) += (args.rng_low <= args.arr1[args.a] &&
            args.arr1[args.a] <= args.rng_high);
    /*
    if (args.rng_low <= args.arr1[args.a] &&
                    args.arr1[args.a] <= args.rng_high) {
        printf("match at [%d, %d]\n", args.a, args.a);
    }
    */
    return args.arr1[args.a];
  }

  if (args.b < args.a)
      return 0;

//printf("a {%d} b {%d} b/2 {%d} b/2 + 1 {%d}\n", args.a, args.b, args.b/2, args.b/2 + 1);

  int left_sum = n2_alg((alg_args){
        .a=args.a,
        .b=(args.a + args.b)/2,
        .arr1=args.arr1,
        .arr2=args.arr2,
        .rng_low=args.rng_low,
        .rng_high=args.rng_high,
        .count=args.count,
        .length=args.length
  });

  int right_sum = n2_alg((alg_args){
        .a=(args.a + args.b)/2 + 1,
        .b=args.b,
        .arr1=args.arr1,
        .arr2=args.arr2,
        .rng_low=args.rng_low,
        .rng_high=args.rng_high,
        .count=args.count,
        .length=args.length
  });

  int count = n2_merge(args);
  int tsum = left_sum + right_sum;
  count += (args.rng_low <= tsum && tsum <= args.rng_high);
  /*
  if (args.rng_low <= tsum &&
                tsum <= args.rng_high) {
    printf("match at [%d, %d]\n", args.a, args.b);
  }
  */
  *(args.count) += count;

  return tsum;
}

int main(void) {
  struct std_in_args {
    int len;
    int* array;
    int bounds[2];
    char buff[1024];
  } args;

  fgets(args.buff, 1024, stdin);
  char* token = strtok(args.buff, "\n");
  args.len = atoi(args.buff);

  fgets(args.buff, 1024, stdin);
  args.array = malloc(sizeof(int)*args.len);
  token = strtok(args.buff, " ");
  int i = 0;
  while(token != NULL && token[0] != '\n') {
    args.array[i] = atoi(token);
    token = strtok(NULL, " ");
    i++;
  }

  fgets(args.buff, 1024, stdin);
  token = strtok(args.buff, " ");
  args.bounds[0] = atoi(token);
  token = strtok(NULL, " ");
  args.bounds[1] = atoi(token);

  int* sum_array = malloc(sizeof(int)*args.len);
  int sum = args.array[0];
  for (i = 1; i < args.len; i++) {
    sum_array[i] = sum+sum_array[i];
  }

  int count = 0;
  n2_alg((alg_args){
        .a=0,
        .b=args.len,
        .arr1=args.array,
        .arr2=sum_array,
        .rng_low=args.bounds[0],
        .rng_high=args.bounds[1],
        .count=&count,
        .length=args.len
  });

  printf("%d\n", count);

  free(args.array);
  free(sum_array);
  return 0;
}
