from pycallgraph import PyCallGraph
from pycallgraph.output import GraphvizOutput

# interesting programming language syntax (what if we could bind constants in recursion)
# - thus we call recurse_solve.bind(words, line_width) -- recurse_solve(start_index) -- similar to J'S
MEM = {}
MEM2 = {}
def recurse_solve(words, line_width, start_index):
    '''
    Solve by starting at the back. Any solution consists of the following problems.
    Use (1-k) number of words left [depends on the number of words left and M].
    Afterwards you are left with N'=(N-l) number of words left. Their should be
    an optimal value for the number of words left, how to place them as this formulation
    allows copying and pasting if a more optimal solution exists. You start at the back
    because the last line does not count in scoring, thus, we cannot use the copy and
    paste argument if we start from the front.
    '''
    def _valid_words_for_line():
        current_amt_left = line_width
        for end_index in range(start_index, len(words)):
            current_amt_left -= (len(words[end_index]) + 1)
            if current_amt_left >= 0:
                yield end_index, current_amt_left
            else:
                return

    if start_index == len(words):
        return 0
    elif MEM.get(len(words) - (start_index+1)) is not None:
        return MEM[len(words) - (start_index+1)]

    min_raggedness = 2**31
    min_raggedness_end_index = -1
    for end_index, current_amt_left in _valid_words_for_line():
        if current_amt_left < 0:
            print('err')
        words_left = len(words) - end_index - 1

        current_value = 0
        if MEM.get(words_left) is not None:
            current_value = MEM[words_left]
        else:
            current_value = \
                recurse_solve(words, line_width, end_index+1)

        # if start_index != 0:
        #     # print(current_amt_left**3)
        current_value += current_amt_left**3

        if current_value < min_raggedness:
            min_raggedness = current_value
            min_raggedness_end_index = end_index

    MEM[len(words) - (min_raggedness_end_index+1)] = min_raggedness
    MEM2[start_index] = min_raggedness_end_index
    return min_raggedness

def min_ragged_print(words, line_width=80):
    with PyCallGraph(output=GraphvizOutput()):
        return recurse_solve(words, line_width, 0)

def main():
    words = open('words.txt').read().split(' ')
    words = [i.strip() for i in words if i != '' and i != '\n']
    words = ['test', 'test']

    res = min_ragged_print(list(reversed(words)))
    print(res)

    index = 0
    path = []
    print(MEM)
    print(MEM2)
    exit()
    while(index != len(words) - 1):
        path.append(index)
        index = MEM2[index]
    print(path)

    prev = 0
    for index in path[1:]:
        print(' '.join(words[prev:index-1]))
        prev = index - 1
    print(' '.join(words[prev:]))

if __name__ == '__main__':
    main()
