#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<stdbool.h>
#include<math.h>

struct CharFixedArray {
  char** array;
  int len;
} typedef Paragraph;

struct Range {
  int start_index;
  int end_index;
} typedef Range;

void update_range(Range* range, int start_index, int end_index) {
  range->start_index = start_index;
  range->end_index = end_index;
}

struct IntFixedArray {
  int* array;
  int len;
} typedef IntArray;

struct RangeFixedArray {
  Range* array;
  int len;
} typedef RangeArray;

void reverse_paragraph(Paragraph* words) {
  char* temp;
  int i;
  for (i = 0; i < (words->len / 2); i++) {
    temp = words->array[i];
    words->array[i] = words->array[words->len - (i + 1)];
    words->array[words->len - (i + 1)] = temp;
  }
}

void reverse_int_array(IntArray* words) {
  int temp;
  int i;
  for (i = 0; i < (words->len / 2); i++) {
    temp = words->array[i];
    words->array[i] = words->array[words->len - (i + 1)];
    words->array[words->len - (i + 1)] = temp;
  }
}

void print_paragraph(Paragraph* words, IntArray* new_line_points) {
  int j = 1;
  int i = 0;
  for (i = 0; i < words->len; i++) {
    if (i == new_line_points->array[j]) {
      printf("\n");
      j++;
    }

    if (i + 1 == new_line_points->array[j])
      printf("%s", words->array[i]);
    else
      printf("%s ", words->array[i]);
  }
  printf("\n");
}

void test_print(Paragraph* words) {
  IntArray spacing = {
    .array=malloc(sizeof(int)*100),
    .len=100
  };

  /* five words per line */
  int i = 0;
  for (i = 1; i < 100; i++) {
    spacing.array[i-1] = 5*i;
  }

  print_paragraph(words, &spacing);
}

struct RaggedPrintResult {
  int cost;
  IntArray* optimal_split_path;
} typedef RaggedPrintResult;

#define FITS_ON_LINE(line_len, word_len) (((int)line_len - (int)word_len) >= 0)
#define NEW_LINE_LENGTH(line_len, word_len) ((int)line_len - (int)word_len)
#define MIN(old, new) (((old) < (new)) ? old : new)
#define MIN_END_INDEX(old, new, old_index, new_index) (((old) < (new)) ? old_index : new_index)
#define RAGGEDNESS(line_length) (pow(line_length, 3))
RaggedPrintResult min_ragged_print(Paragraph* words, int line_width) {
  IntArray table = {
    .array=malloc(sizeof(int)*(words->len)),
    .len=words->len
  };

  RangeArray min_path = {
    .array=malloc(sizeof(Range)*(words->len)),
    .len=words->len
  };

  int i = 0;
  for (i = 0; i < (words->len - 1); i++) {
    int min_raggedness = 2<<20;
    int min_index = -1;
    int word = i;
    int line_length = line_width;
    while (word >= 0
           && FITS_ON_LINE(line_length, strlen(words->array[word]))) {
      line_length = NEW_LINE_LENGTH(line_length, strlen(words->array[word]));

      min_raggedness = MIN(min_raggedness,
                           RAGGEDNESS(line_length) +
                           (((word - 1) >= 0) ? table.array[word - 1] : 0));
      min_index = MIN_END_INDEX(min_raggedness,
                                RAGGEDNESS(line_length) +
                                (((word - 1) >= 0) ? table.array[word - 1] : 0),
                                min_index, word);
      word--;
      line_length--; /* subtract <space> after calculation */
    }

    table.array[i] = min_raggedness;
    update_range(&min_path.array[i], min_index, i);
  }

  i = words->len - 1;
  int min_raggedness = 2<<20;
  int min_index = -1;
  int word = i;
  int line_length = line_width;
  while (word >= 0 &&
         FITS_ON_LINE(line_length, strlen(words->array[word]))) {
    line_length = NEW_LINE_LENGTH(line_length, strlen(words->array[word]));

    min_raggedness = MIN(min_raggedness,
                         (((word - 1) >= 0) ? table.array[word - 1] : 0));
    min_index = MIN_END_INDEX(min_raggedness,
                              (((word - 1) >= 0) ? table.array[word - 1] : 0),
                              min_index, word);

    word--;
    line_length--; /* subtract <space> after calculation */
  }
  table.array[i] = min_raggedness;
  update_range(&min_path.array[i], min_index, i);

#ifdef DEBUG_PRINT
  for (i = 0; i < min_path.len; i++) {
    if (i % 5 == 0) {
      printf("\n");
    }

    printf("%d ", table.array[i]);
  }
  printf("\n");

  for (i = 0; i < min_path.len; i++) {
    if (i % 5 == 0) {
      printf("\n");
    }

    printf("(%d, %d)", min_path.array[i].start_index, min_path.array[i].end_index);
  }
  printf("\n");

  printf("min: %d with %d\n", min_index, min_raggedness);
#endif /* DEBUG_PRINT */

  IntArray* optimal_split_path = malloc(sizeof(IntArray));
  optimal_split_path->array=malloc(sizeof(int)*words->len),
  optimal_split_path->len=words->len;
  /* set last index (once reversed) to length to help print
     skip printing space at the end of the line */
  optimal_split_path->array[0] = words->len;

  int start_index = min_path.len - 1;
  int path_len = 1;
  optimal_split_path->array[path_len] = start_index + 1;
  path_len++;
  while (start_index != -1) {
    start_index = min_path.array[start_index].start_index - 1;
    optimal_split_path->array[path_len] = start_index + 1;
    path_len++;
  }
  optimal_split_path->len = path_len;
  reverse_int_array(optimal_split_path);

  return (RaggedPrintResult) {
    .cost=table.array[table.len - 1],
    .optimal_split_path=optimal_split_path
  };
}

void test_fill_paragraph(Paragraph* words) {
  char buff[256];
  int char_count = 0;
  int i = 0;
  for (i = 0; i < words->len; i++) {
    char_count = sprintf(buff, "%d", i);
    words->array[i] = malloc(sizeof(char)*(char_count + 1));
    strcpy(words->array[i], buff);
  }
}

/* make sure to reverse array */
int main() {
  char buff[256];
  int num_words = -1;
  int line_length = -1;
  int i = 0;
  scanf("%d %d", &num_words, &line_length);

  Paragraph words = {
    .array=malloc(sizeof(char*)*num_words),
    .len=num_words
  };

  for (i = 0; i < num_words; i++) {
    scanf("%s", buff);
    int len = strlen(buff) + 1;
    words.array[i] = malloc(sizeof(char)*len);
    strcpy(words.array[i], buff);
  }

/* #ifdef DEBUG_PRINT */
/*   int num_words = 100000; */
/*   int line_length = 80; */
/*   Paragraph words = { */
/*     .array=malloc(sizeof(char*)*num_words), */
/*     .len=num_words */
/*   }; */

/*   test_fill_paragraph(&words); */
/*   printf("Before reverse...\n"); */
/*   test_print(&words); */
/*   printf("Reverse, reverse...\n"); */
/*   reverse_paragraph(&words); */
/*   test_print(&words); */
/*   printf("Reverse, reverse...\n"); */
/*   reverse_paragraph(&words); */
/*   test_print(&words); */
/* #endif /\* DEBUG_PRINT *\/ */

  RaggedPrintResult results = min_ragged_print(&words, line_length);

#ifdef DEBUG_PRINT
  printf("Cost: %d\n", results.cost);
  for (i = 0; i < results.optimal_split_path->len; i++) {
    if (i % 5 == 0)
      printf("\n");
    if (i + 1 == results.optimal_split_path->len)
      printf("%d", results.optimal_split_path->array[i]);
    else
      printf("%d->", results.optimal_split_path->array[i]);
  }
  printf("\n");
#endif /* DEBUG_PRINT */

  printf("%d\n", results.cost);
  print_paragraph(&words, results.optimal_split_path);

  free(words.array);
  return 0;
}
