Author: Austin Minor (米诺)
Project: 3 - Partition Number
Compile Instructions:
+ Numpy must be installed.
+ + Typically this means using the following command or some variation of it `pip install numpy`
Usage Instructions:
+ Python 2 was used as the execution environment.
+
+ The usage is simply
+ - `python partition.py <N>`
+ - where <N> is the number you wish to partition
+
+ Note this program is meant to be ran from a terminal.
+ Note this program was only tested in a Ubuntu 18.04 terminal.
Files:
+ partition.py
+ + program
+ p3.pdf
+ + Rendered description document.
+ p3.doc
+ + Source description document.
+ readme.txt
+ + This file.
+ archive
+ + p3.py
+ + + First version of the program written to use Sympy
