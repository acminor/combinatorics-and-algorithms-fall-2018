import numpy as np
import numpy.polynomial.polynomial as py
from multiprocessing import Pool, cpu_count
from math import ceil

def create_par_divisor(partition_number, start, end):
    poly = np.zeros(partition_number+1)
    poly[0] = 1

    temp = np.zeros(partition_number+1)
    temp[0] = 1
    for i in range(start, end):
        # poly *= (1 - x**i)
        temp[i] = -1
        poly = py.polymul(poly,temp)
        temp[i] = 0
    return poly

def par_op(part_num, div_part):
    num = np.zeros(partition_number+1)
    num[0] = 1

    temp = np.zeros(partition_number+1)
    for i in range(0, partition_number):
        em = np.nonzero(num)[0][0]
        ec = num[em]
        temp[em] = ec
        pn = ec
        print("{} : {}".format(int(pn)))
        num = py.polysub(num, py.polymul(temp, denom))
        temp[em] = 0
    return pn

THRESHOLD = 100
def _apply(args):
    part_num, start, end = args
    return create_par_divisor(part_num, start, end)
def _create_divisor(partition_number):
    if partition_number > THRESHOLD:
        num_parts = int(ceil(partition_number / THRESHOLD))
        ranges = [(partition_number, i*THRESHOLD,
                   (i+1)*THRESHOLD) for i in range(0, num_parts - 1)]
        final_start = (num_parts - 1) * THRESHOLD
        final_end = partition_number
        ranges.append((partition_number, final_start, final_end))
        ranges[0] = (partition_number, 1, THRESHOLD)
        x = Pool()
        v = x.map(_apply, ranges)

        poly = np.zeros(partition_number+1)
        poly[0] = 1
        for i in v:
            poly = py.polymul(i, poly)
        return poly
    else:
        return create_divisor(partition_number)


def create_divisor(partition_number):
    poly = np.zeros(partition_number+1)
    poly[0] = 1

    temp = np.zeros(partition_number+1)
    temp[0] = 1
    for i in range(1, partition_number+1):
        # poly *= (1 - x**i)
        temp[i] = -1
        poly = py.polymul(poly,temp)
        temp[i] = 0
    return poly

def solve(partition_number):
    if partition_number == 0:
        return 1

    partition_number += 1

    num = np.zeros(partition_number+1)
    num[0] = 1
    denom = _create_divisor(partition_number)

    temp = np.zeros(partition_number+1)
    for i in range(0, partition_number):
        #em and ec are the terms used in Sympy
        #to denote the coefficient and degree of a term
        #- we use the same terminology here as this code is a descendant of Sympy prototype
        # em = degree
        # ec = coefficient
        em = np.nonzero(num)[0][0]
        ec = num[em]
        temp[em] = ec
        pn = ec
        num = py.polysub(num, py.polymul(temp, denom))
        temp[em] = 0
    print("{} : {}".format(i, int(pn)))
    return pn

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Computes all partition numbers from 1 to n.')
    parser.add_argument('n', type=int)
    args = parser.parse_args()

    solve(args.n)
