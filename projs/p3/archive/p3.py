from sympy import *
from sympy.abc import x
from multiprocessing import Pool, cpu_count
from math import ceil
import pickle

def create_divisor(partition_number):
    poly = x**0
    for i in range(1, partition_number+1):
        poly *= (1 - x**i)
    poly = poly.as_poly()
    return poly

# def _solve_solve(args):
#     split_num, denom, em, ec = args
#     num = split_num - (em*ec)*denom
#     return num

def solve(partition_number):
    if partition_number == 0:
        return 1

    # def par_solve(num, denom, em, ec):
    #     l = Pool()
    #     t = num.as_dict().items()
    #     input = [(Poly(
    #         dict(
    #             t[int(i*len(t)/float(cpu_count())):int((i+1)*ceil(len(t)/float(cpu_count())))]
    #         ), x).as_expr(), denom, em, ec)
    #              for i in range(0, int(ceil(len(t)/float(cpu_count()))))]
    #     print(int(ceil(len(t)/float(cpu_count()))))
    #     # print(len(input))
    #     res = l.map(_solve_solve, input)

    #     out = res[0]
    #     for i in range(1, len(t)/cpu_count()):
    #         out += res[i]
    #     out -= int(ceil(len(t)/float(cpu_count())) + 1)*denom
    #     return out

    num = Poly('1', x)
    denom = create_divisor(partition_number)

    pn = x**0
    for i in range(0, partition_number):
        em = num.EM().as_expr()
        ec = num.EC().as_expr()
        pn = ec
        old_num = num

        # if i == 9:
        #     t = num.as_dict().items()
        #     t1 = Poly(dict(t[0:int(len(t)/2)]), x)
        #     t = Poly(dict(t[int(len(t)/2):]), x)
        #     t = t - (em*ec)*denom
        #     t1 = t1 - (em*ec)*denom
        #     print(t+t1)
        num = num - (em*ec)*denom
        # num = par_solve(num, denom, em, ec)
    return pn

# x = Pool()
# def _print(x):
#     print(x)
# h = x.map(solve, range(0,10))
#
# print(h)

print(solve(60))
