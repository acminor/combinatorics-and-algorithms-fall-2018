(TeX-add-style-hook
 "p1"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem") ("algorithm2e" "linesnumbered" "lined" "boxed")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "report"
    "rep10"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref"
    "tikz"
    "caption"
    "subcaption"
    "algorithm2e")
   (LaTeX-add-labels
    "sec:org4a2c3fd"
    "sec:orgfe30c09"
    "sec:org944a456"
    "sec:org8864eef"
    "sec:org7f9fd28"
    "sec:orgca9a8b5"
    "sec:org878a51b"
    "sec:orge93c951"
    "sec:orgdc4df5f"
    "fig:alg-line-ex"
    "sec:org7d907f7"
    "sec:org84ae614"
    "sec:orgd55d50f"
    "sec:org92a4d16"
    "sec:orgb6d7447"
    "sec:org40de429"
    "sec:org3cf9f6b"
    "sec:org5d80d83"
    "sec:org7575004"
    "sec:org7e2acc4"))
 :latex)

