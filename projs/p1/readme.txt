Author: Austin Minor (米诺)
Project: 1-Android Cellphone Passcodes
Compile Instructions:
+ Note: This program is meant to be compiled on Linux.
+ - It has been tested to work on Ubuntu 18.04.
+
+ Compile Command: gcc -o passcodes android.c -lm
Usage Instructions:
+ The application will attempt to calculate and print information
+ relating to all the possible combinations for Android grids up
+ to a 4x4 grid.
+
+ As analyzed in our report, a 4x4 grid is not conveniently compute-able.
+ Thus to end the program early, one can use Ctrl-C in a Linux terminal.
+ All the relativant information for a 1x1, 2x2, and 3x3 grid will already
+ be printed.
+
+ Run Command: ./passcodes
+
+ Note: This program is meant to be ran from a Linux terminal.
Files:
+ android.c
+ - Source code for swipe calculation.
+ android-combos.py
+ - Prototype of initial application (included for reference).
+ p1.org
+ - Source code for PDF document (may or may not have non-original
+ - code as this was used for typesetting).
+ p1.pdf
+ - Main document.
+ data.xls
+ - Spreadsheet of charts and data.
+ screenshots
+ - Folder with images of recorded data.
+ *.png
+ - Various images used in document.
+ readme.txt
+ - Readme.
+ passcodes
+ - Executable for the application. Compiled on Ubuntu 18.04.
+ *
+ - Other files used in this project (including temporary files).
