// Needs POSIX time.h
#include<time.h>
// LibC
#include<math.h>
#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>

#define TRUE 1
#define FALSE 0

struct {
  uint64_t grid;
  uint8_t width;
} typedef AGrid;

struct {
  uint8_t x;
  uint8_t y;
} typedef Pair;

AGrid* new_agrid(int width) {
  AGrid* grid = malloc(sizeof(AGrid));
  grid->grid = 0;
  grid->width = width;

  return grid;
}

uint8_t _AGrid_cal_position(uint8_t x, uint8_t y, uint8_t width) {
  return (uint8_t) (x + width*y);
}

Pair _AGrid_cal_pair(uint8_t raw_index, uint8_t width) {
  Pair pair;
  pair.y = raw_index / width;
  pair.x = raw_index % width;
  return pair;
}

uint8_t _AGrid_cal_x_y(uint8_t x, uint8_t y, uint8_t width) {
  return (uint8_t) (x + width*y);
}

void AGrid_set_color(AGrid* grid, uint8_t x, uint8_t y) {
  uint8_t offset = _AGrid_cal_position(x, y, grid->width);
  grid->grid |= 1 << offset;
}

int AGrid_get_color(AGrid* grid, uint8_t x, uint8_t y) {
  uint8_t offset = _AGrid_cal_position(x, y, grid->width);
  return (grid->grid & (1 << offset));
}

void AGrid_clear_color(AGrid* grid, uint8_t x, uint8_t y) {
  uint8_t offset = _AGrid_cal_position(x, y, grid->width);
  grid->grid = grid->grid & ~(1 << offset);
}

int _AGrid_bounds_check(int8_t x, int8_t y, uint8_t width) {
  return (x >=0 && x < width &&
          y >=0 && y < width);
}

int AGrid_neighbors(AGrid* grid, Pair** neighbors_in, uint8_t x, uint8_t y) {
  Pair* neighbors = malloc(9*sizeof(Pair));
  uint8_t index = 0;

  // For all elements get all potential connections
  for(int i = 0; i < pow(grid->width, 2); i++) {
    Pair potential_connection = _AGrid_cal_pair(i, grid->width);

    // do not consider yourself as a potential connection
    if (potential_connection.x == x && potential_connection.y == y) continue;

    // only consider connections that are uncolored
    // - skip colored connection
    if (AGrid_get_color(grid, potential_connection.x, potential_connection.y) != 0) continue;

    /*
      Try to search for blocking connections

      if one: then cannot use as neighbor
      if not one: then can use as neighbor
    */

    // if vertical line above
    if ((x - potential_connection.x) == 0) {
      int is_valid = TRUE;

      // if below
      if (potential_connection.y > y) {
        // if any element below and between potential connection
        // is not colored cannot use potential connection
        for (int _y = y + 1; _y < potential_connection.y; _y++) {
          if (AGrid_get_color(grid, x, _y) == 0) {
            is_valid = FALSE;
          }
        }
      } else { // if above
        // if any element above and between potential connection
        // is not color cannot use potential connection
        for (int _y = y - 1; _y > potential_connection.y; _y--) {
          if (AGrid_get_color(grid, x, _y) == 0) {
            is_valid = FALSE;
          }
        }
      }

      if (is_valid) {
        neighbors[index].x = potential_connection.x;
        neighbors[index].y = potential_connection.y;
        index++;
      }

      continue; // skip below code because done
    }

    double CORD_Y_CORRECT = -1; // since we are doing things in a different cord system

    // calculate line (y=mx+b) between potential connection and starting point
    double slope = (CORD_Y_CORRECT*((double)y - potential_connection.y))
      / ((double)x - potential_connection.x);
    double y_int = CORD_Y_CORRECT*(y+.5) - slope*(x+.5);

    // for all elements in grid check if a blocker blocks the potential connection
    int is_valid = TRUE;
    for (int j = 0; j < pow(grid->width, 2); j++) {
      Pair potential_blocker = _AGrid_cal_pair(j, grid->width);

      // if j == i then potential connection == potential blocker
      // - skip
      // if potential block equals starting point
      // - skip
      if (j == i || (potential_blocker.x == x && potential_blocker.y == y)) continue;

      // check top left to bottom right diagonal intersect
      double tl_slope = -1;
      double tl_y_int = CORD_Y_CORRECT * potential_blocker.y - tl_slope*potential_blocker.x;

      // calculate the intersection of the line between
      // start and potential connection and the top-left to bottom right diagonal line
      double tl_x_int = (y_int - tl_y_int)/(tl_slope - slope);

      // check top right to bottom left diagonal intersect
      double tr_slope = 1;
      double tr_y_int = CORD_Y_CORRECT * potential_blocker.y - tr_slope*(potential_blocker.x+1);

      // calculate the intersection of the line between
      // start and potential connection and the top-right to bottom left diagonal line
      double tr_x_int = (y_int - tr_y_int)/(tr_slope - slope);

      // tolerance value for floating point errors
      double tol = 0.0;

      // get the right direction for comparison
      int upper = (x > potential_connection.x) ? x : potential_connection.x;
      int lower = (x < potential_connection.x) ? x : potential_connection.x;

      int line_crosses_tr = tr_x_int > (potential_blocker.x + .4) - tol &&
        tr_x_int < (potential_blocker.x + .6 + tol) &&
        // check more
        tr_x_int > (lower + .5 - tol) &&
        tr_x_int < (upper +.5 + tol);
      int line_crosses_tl = tl_x_int > (potential_blocker.x + .4) - tol &&
        tl_x_int < (potential_blocker.x + .6 + tol) &&
        // check more
        tl_x_int > (lower + .5 - tol) &&
        tl_x_int < (upper +.5 + tol);

      // if it crosses either line and the blocker is uncolored
      // it is not a valid point
      if ((line_crosses_tl || line_crosses_tr) &&
          (AGrid_get_color(grid, potential_blocker.x, potential_blocker.y) == 0)) {
        is_valid = FALSE;
        break;
      }
    }

    if (is_valid) {
      neighbors[index].x = potential_connection.x;
      neighbors[index].y = potential_connection.y;
      index++;
    }
  }

  *neighbors_in = neighbors;

  return index;
}

struct {
  uint64_t count;
  uint8_t cur_path_len;
  struct timespec time;
} typedef Stats;

Stats static_stats() {
  Stats stats;
  stats.count = 0;
  //  stats.count_paths_len = 0;
  stats.cur_path_len = 0;
  clock_gettime(CLOCK_REALTIME, &stats.time);

  return stats;
}

void _recursive_dfs(AGrid* grid, Stats* stats, uint8_t x, uint8_t y) {
  AGrid_set_color(grid, x, y);
  stats->cur_path_len += 1;

  if (stats->cur_path_len >= 4) {
    stats->count += 1;
  }

  Pair* neighbors = NULL;
  uint8_t num_neighbors = AGrid_neighbors(grid, &neighbors, x, y);

  for(uint8_t i = 0; i < num_neighbors; i++) {
    _recursive_dfs(grid, stats, neighbors[i].x, neighbors[i].y);
  }

  AGrid_clear_color(grid, x, y);
  stats->cur_path_len -= 1;
  free(neighbors);
}

Stats enumerate_paths(uint8_t width) {
  Stats stats = static_stats();
  AGrid* grid = new_agrid(width);

  for(uint8_t i = 0; i < width; i++) {
    for(uint8_t j = 0; j < width; j++) {
      int levelc = stats.count;
      _recursive_dfs(grid, &stats, i, j);
    }
  }


  struct timespec finish_time;
  clock_gettime(CLOCK_REALTIME, &finish_time);
  long nanotime = finish_time.tv_nsec - stats.time.tv_nsec;
  stats.time.tv_nsec= nanotime;

  return stats;
}

Stats quick_compute(uint8_t width) {
  Stats stats = static_stats();
  AGrid* grid = new_agrid(width);

  int running_width = width;
  int level = 0;

  Pair offset_point;
  uint64_t prev_count;
  int offset;
  while (running_width > 1) {
    offset = level*(width + 1);

    prev_count = stats.count;
    offset_point = _AGrid_cal_pair(offset, width);
    _recursive_dfs(grid, &stats, offset_point.x, offset_point.y);
    stats.count += 3*(stats.count - prev_count);

    for (int i = 1; i < running_width/2; i++) {
      prev_count = stats.count;
      offset_point = _AGrid_cal_pair(i + offset, width);
      _recursive_dfs(grid, &stats, offset_point.x, offset_point.y);
      stats.count += 7*(stats.count - prev_count);
    }

    if (running_width % 2 != 0) {
      prev_count = stats.count;
      offset_point = _AGrid_cal_pair((int) ceil(width/2.0) - 1 + offset, width);
      _recursive_dfs(grid, &stats, offset_point.x, offset_point.y);
      stats.count += 3*(stats.count - prev_count);
    }

    running_width -= 2;
    level += 1;
  }

  if (running_width == 1) {
    offset = level*(width + 1);
    offset_point = _AGrid_cal_pair(offset, width);
    _recursive_dfs(grid, &stats, offset_point.x, offset_point.y);
  }

  struct timespec finish_time;
  clock_gettime(CLOCK_REALTIME, &finish_time);
  long nanotime = finish_time.tv_nsec - stats.time.tv_nsec;
  stats.time.tv_nsec= nanotime;

  return stats;
}

int main(int argv, char** argc) {
  Stats stats;
  for (int i = 1; i < 5; i++) {
    printf("+++++ Graph Size %i +++++\n", i);
    printf("+++++ Fast Compute  +++++\n");
    stats = quick_compute(i);
    printf("Count: %li\n", stats.count);
    printf("Time: %f\n", stats.time.tv_nsec/(pow(10.0, 9)));
    printf("+++++ Slow Compute  +++++\n");
    stats = enumerate_paths(i);
    printf("Count: %li\n", stats.count);
    printf("Time: %f\n", stats.time.tv_nsec/(pow(10.0, 9)));
    printf("+++++++++++++++++++++++++\n");
  }
}
