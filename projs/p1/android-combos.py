from time import time
import math

class GraphNode:
    CLEAR_COLOR = -1
    def __init__(self):
        # technically allows parallel edges (assumes graph construction is proper)
        self._edges = []
        self._color = self.CLEAR_COLOR
    def add_edge(self, node):
        self._edges.append(node)
    def get_edges(self):
        return self._edges
    def set_color(self, color):
        self._color = color
    def get_color(self):
        return self._color
    def clear_color(self):
        self._color = self.CLEAR_COLOR
    def __repr__(self):
        return "x"

class Graph:
    def __init__(self):
        # use array for benefits later
        self._nodes = []
    def add_node(self, node):
        self._nodes.append(node)
    def get_nodes(self):
        return self._nodes
    def _set_nodes(self, nodes):
        self._nodes = nodes
    def get_num_nodes(self):
        return len(self._nodes)

def create_android_swipe_graph(size=3):
    def xy_index(x, y):
        return x + y*size
    def adjacent_nodes(x, y):
        def bounds_check(pair):
            return pair[0] >= 0 and pair[0] < size \
               and pair[1] >= 0 and pair[1] < size \
               and (not (pair[0] == x and pair[1] == y))

        nine_dof = [(x-1, y-1), (x, y-1), (x+1, y-1),
                    (x-1,   y), (x,   y), (x+1,   y),
                    (x-1, y+1), (x, y+1), (x+1, y+1)]

        return filter(bounds_check, nine_dof)

    swipe_grid = [GraphNode() for i in range(0, size*size)]

    total_edges = 0
    for i in range(0, size):
        for j in range(0, size):
            current_node = swipe_grid[xy_index(i,j)]
            for edge in adjacent_nodes(i,j):
                total_edges += 1
                adj_node_x = edge[0]
                adj_node_y = edge[1]
                _index = xy_index(adj_node_x, adj_node_y)
                current_node.add_edge(swipe_grid[_index])

    graph = Graph()
    graph._set_nodes(swipe_grid)
    return graph

    # print(total_edges/2) # hand-shaking lemma

def enumerate_paths(graph):
    class Stats:
        def __init__(self):
            self.clr()

        def clr(self):
            self.count = 0
            self.time = time()
            self.cur_path_len = 0
            self.cur_path = []
            self.count_paths_len = dict()

        def inc_count_path_len(self):
            if self.count_paths_len.get(self.cur_path_len) is None:
                self.count_paths_len[self.cur_path_len] = 1
            else:
                self.count_paths_len[self.cur_path_len] += 1

    this = enumerate_paths
    this.stats = Stats()

    CUR_VISIT_COLOR = 10

    def _recursive_dfs(node):
        node.set_color(CUR_VISIT_COLOR)
        this.stats.cur_path.append(node)
        this.stats.count += 1
        this.stats.cur_path_len += 1
        this.stats.inc_count_path_len()

        for connected_node in node.get_edges():
            if connected_node.get_color() == CUR_VISIT_COLOR:
                pass
            else:
                _recursive_dfs(connected_node)

        node.clear_color()
        this.stats.cur_path.pop()
        this.stats.cur_path_len -= 1

    # choose all starting nodes
    this.stats.clr()
    pc = 0
    for node in graph.get_nodes():
        _recursive_dfs(node)
        print("Count {}".format(this.stats.count))
        pc = this.stats.count
    tot_time = time() - this.stats.time

    print("Slow Count: {}".format(this.stats.count))
    print("Slow Count - Len: {}".format(this.stats.count_paths_len))
    print("Slow Count - Time: {}".format(tot_time))


    def quick_compute(graph):
        n = int(math.sqrt(graph.get_num_nodes()))
        original_n = int(math.sqrt(graph.get_num_nodes()))
        level = 0

        while(n > 1):
            offset = level*(original_n + 1)

            prev_count = this.stats.count
            _recursive_dfs(graph.get_nodes()[offset])
            this.stats.count += 3*(this.stats.count - prev_count)

            for i in range(1, int(math.floor(n/2))):
                prev_count = this.stats.count
                _recursive_dfs(graph.get_nodes()[i + offset])
                this.stats.count += 7*(this.stats.count - prev_count)

            if math.floor(n/2) != math.ceil(n/2):
                prev_count = this.stats.count
                _recursive_dfs(graph.get_nodes()[int(math.ceil(n/2) - 1) + offset])
                this.stats.count += 3*(this.stats.count - prev_count)

            n = n - 2
            level += 1

        if n == 1:
            offset = level*(original_n + 1)
            prev_count = this.stats.count
            _recursive_dfs(graph.get_nodes()[int(math.ceil(n/2) - 1) + offset])

    this.stats.clr()
    quick_compute(graph)
    print("Fast Count: {}".format(this.stats.count))
    print("Fast Count - Time: {}".format(time()-this.stats.time))

for i in range(1, 5):
    print("++++ GRAPH SIZE {} ++++".format(i))
    graph = create_android_swipe_graph(i)
    enumerate_paths(graph)
    print("+++++++++++++++++++++++")

print("++++ iPhone Calculation (4-pin) ++++")
print("Count: {}".format(10**4))
print("++++++++++++++++++++++++++++++++++++")
