import sys

data = sys.stdin.read().split('\n')

perms = [[int(i) for i in perm.split(',')]
            for perm in [val for val in data if val.find(',') != -1]]

print('\n'.join(data))
print("Contains duplicates: {}".format(
    'yes' if (len(perms) != len(set([','.join([str(j) for j in i]) for i in perms])))
    else 'no'))
