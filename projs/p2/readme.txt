Author: Austin Minor (米诺)
Project: 2-SJT Ordering
Compile Instructions:
+ Note: This program has only been tested to compile on Linux.
+ - No guarantees to it working on anything other than Linux.
+ - Successfully compiled and ran on Ubuntu 18.04.
+
+ Compile Command: gcc -o permutation p2.c -lm
Usage Instructions:
+ This application provides multiple different permutation generators
+ along with measurements to their efficiency.
+
+ It is instructive to look at usage of the command from the command line.
+
+ <<Taken from program>>
+ Usage: ./program permutation-size algorithm print-results?
+ 
+ Must supply a target size for the permutation.
+ Must supply a target algorithm for the permutation.
+ --
+ 0- Lexicographical Ordering
+ 1- SJT Ordering
+ 2- Recursive Ordering
+ 5- All algorithms.
+ --
+ <<End>>
+
+ This shows how to run a command.
+ 
+ Note that printing results can mess up benchmarks. Thus
+ the option to disable it form the tool was added.
+
+ Note program is the name of the program (in our case permutation).
+
+ Example (SJT for size 10, print permutations): ./permutation 10 1 1
+
+ Note this program is meant to be ran from a Linux terminal.
Files:
+ data.pl
+ - Perl script to automate the collection of run-time statistics of the program.
+ dat.csv
+ - Output data from the copied from the output of the Perl script.
+ dat.xls
+ - Analysis of statistics data (dat.csv) in a spreadsheet program.
+ p2.c
+ - Permutation program source code.
+ permutation
+ - Linux 64-bit compiled program from p2.c
+ p2.org
+ - Source file for project documentation.
+ p2.tex
+ - Incremental compilation of p2.org before transformation into pdf document.
+ p2.pdf
+ - Documentation of project.
+ test.py
+ - Python script to test that permutations are unique. Can be used to test output
+   of the permutation program.
+ time.png
+ - Graph of the time to compute permutations for the various algorithms.
+ timeS.png
+ - Graph of the time to compute permutations for the various algorithms
+   (sub-setted to a different range).
+ count.png
+ - Graph of permutation count of the various algorithms.
+ data.png
+ - Screenshot showing the output of the Perl data collection script.
+ readme.txt
+ - This file.
