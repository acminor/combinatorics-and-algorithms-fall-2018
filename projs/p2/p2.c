/*
 Project 2
 Austin Minor
 Combinatorics

 Brief:
 To analyze and develop multiple permutation producing algorithms.
 End:
 */

#include <time.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

void print_array(int* array, int length) {
  for (int i = 0; i < length; i++) {
    printf("%i%c ", array[i]+1, (i<length-1) ? ',' : ' ');
  }
  puts("");
}

struct {
  int* array;
  unsigned int length;
} typedef Array;

#define NEW_ARRAY(length) \
  ((Array){.length=length, .array=calloc(length, sizeof(int))})

#define SWAP(one, two) \
  one ^= two; \
  two ^= one; \
  one ^= two;

typedef int (*TestFunction)(Array, char);

struct {
  char* test_name;
  TestFunction test_function;
} typedef Test;

int lexographical_ordering(Array array, char isPrint) {
  int length = array.length;
  int* data = array.array;
  int isDone = 0;
  int count = 0;

  while (isDone != 1) {
    isDone++;
    for (int i = length-1; i > 0; i--) {
      if (data[i-1] < data[i]) {
        if (isPrint)
          print_array(data, length);
        count++;
        int min = length;
        int min_index = length-1;
        for (int j = length-1; j >= i; j--) {
          if (data[j] > data[i-1]) {
            min = min < data[j]-data[i-1] ? min : (min_index=j, data[j]-data[i-1]);
          }
        }

        SWAP(data[i-1], data[min_index]);

        for (int j = 0; j < (length-i)/2; j++) {
          SWAP(data[i+j], data[(length - 1) - j]);
        }

        isDone--;
        break;
      }
    }
  }

  if (isPrint)
    print_array(data, length);
  count++;
  return count;
}

#define DIR_LEFT 1
#define DIR_RIGHT 0

#define GET_DIR(array, index)                    \
  ((array[index/sizeof(int)] & (1 << (index%sizeof(int)))) == 0)

#define TOGGLE_DIR(array, index) \
  array[index/sizeof(int)] = (array[index/sizeof(int)] ^ (1 << (index%sizeof(int))))

int sjt_ordering(Array array, char isPrint) {
  int length = array.length;
  int* data = array.array;
  int* data_index_map = NEW_ARRAY(length).array;
  const int max = length - 1;

#define CAN_MOVE_LEFT(index) \
  (index > 0 && data[index] > data[index - 1] && \
   GET_DIR(dir_data, data[index]) == DIR_LEFT)

#define CAN_MOVE_RIGHT(index) \
  (index < (length - 1) && data[index] > data[index + 1] && \
   GET_DIR(dir_data, data[index]) == DIR_RIGHT)

  for (int i = 0; i < length; i++)
    data_index_map[i] = i;

  int* dir_data = NEW_ARRAY(length).array;

  int sweep_index = length - 1;
  int count = 0;
  // largest can always move again after one move by smaller

  if (isPrint) {
    print_array(data, length);
  }
  for (;;) {
    if (CAN_MOVE_LEFT(sweep_index)) {
      SWAP(data[sweep_index], data[sweep_index - 1]);
      SWAP(data_index_map[data[sweep_index]], data_index_map[data[sweep_index - 1]]);

      sweep_index--;
    } else if (CAN_MOVE_RIGHT(sweep_index)) {
      SWAP(data[sweep_index], data[sweep_index + 1]);
      SWAP(data_index_map[data[sweep_index]], data_index_map[data[sweep_index + 1]]);

      sweep_index++;
    } else {
      break;
    }

    count++;
    if (isPrint) {
      print_array(data, length);
    }

    // could make more efficient with more bit tricks X|
    for (int i = data[sweep_index] + 1; i < length; i++)
      TOGGLE_DIR(dir_data, i);

    if (data[sweep_index] != max) {
      sweep_index = data_index_map[max];
    } else if (sweep_index == 0 || sweep_index == length - 1) {
      for (int i = max; i >= 0; i--) {
        if (CAN_MOVE_RIGHT(data_index_map[i]) || CAN_MOVE_LEFT(data_index_map[i])) {
          sweep_index = data_index_map[i];
          break;
        }
      }
    }
  }

  free(dir_data);
  free(data_index_map);

  return count+1;
}

int _recursive_ordering(int* array, int* unchosen, char isPrint, int unchosen_len, int length) {
  if (unchosen_len == 0) {
    if (isPrint)
      print_array(array, length);

    return 1;
  }

  int sum = 0;
  for (int i = 0; i < unchosen_len; i++) {
    array[length] = unchosen[i];
    int* unchosen_new = malloc(sizeof(int)*(unchosen_len - 1));
    for (int j = 0, k = 0; j < unchosen_len; j++, k++) {
      if (i == j) {
        k--;
        continue;
      }
      unchosen_new[k] = unchosen[j];
    }
    sum += _recursive_ordering(array, unchosen_new, isPrint, unchosen_len - 1, length + 1);
    free(unchosen_new);
  }

  return sum;
}

int recursive_ordering(Array array, char isPrint) {
  int length = array.length;
  int* data = array.array;
  int* unchosen = malloc(sizeof(int)*length);
  for (int i = 0; i < length; i++)
    unchosen[i] = i;

  int count = _recursive_ordering(data, unchosen, isPrint, length, 0);
  free(unchosen);

  return count;
}

Test tests[] = {
  {"Lexicographical Ordering", lexographical_ordering},
  {"SJT Ordering", sjt_ordering},
  {"Recursive Ordering", recursive_ordering}
};

void test(int algorithm, int length, char isPrint) {
  int count = 0;
  Array temp = {.length=length, .array=malloc(sizeof(int)*length)};
  for (int i = 0; i < length; i++)
    temp.array[i] = i;

  clock_t start = clock();
  printf("Algorithm: %s\n", tests[algorithm].test_name);
  count = tests[algorithm].test_function(temp, isPrint);
  printf("Total Count: %i\n", count);
  clock_t end = clock();
  printf("Total Time: %f\n", (double) (end-start) / CLOCKS_PER_SEC);

  free(temp.array);
}

int main(int argc, char** argv) {
  char isPrint = 1;
  if (argc < 3) {
    puts("Usage: ./program permutation-size algorithm print-results?\n");
    puts("Must supply a target size for the permutation.");
    puts("Must supply a target algorithm for the permutation.");
    puts("--");
    for (int i = 0; i < 3; i++)
      printf("%i- %s\n", i, tests[i].test_name);
    puts("5- All algorithms.");
    puts("--");

    exit(1);
  } else if (argc == 4) {
    isPrint = atoi(argv[3]);
  }

  int length = atoi(argv[1]);
  int algorithm = atoi(argv[2]);

  if (algorithm == 5) {
    for (int i = 0; i < 3; i++)
      test(i, length, isPrint);
  } else {
    test(algorithm, length, isPrint);
  }
}
