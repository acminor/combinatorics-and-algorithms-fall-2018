$PROG_NAME = 'permutation';
$TRIAL_SIZE = 1000;
$IS_PRINT = 0;

print "PermSize,Algorithm,Count,AvgTotalTime\n";
for $perm_size (1..12) {
    @algs = ('Lexographical Ordering','SJT Ordering','Recursive Ordering');
    for $algIndex (0..$#algs) {
        $out = "";
        $time = 0.0;
        for $trial (0..$TRIAL_SIZE) {
            $out = `./$PROG_NAME $perm_size $algIndex $IS_PRINT`;
            $out =~ /Total Time: (.*)/;
            $time += $1;
        }
        $time = $time / $TRIAL_SIZE;
        $out =~ /Total Count: (.*)/;
        $count = $1;
        print "$perm_size,$algIndex,$count,$time\n";
    }
}
