#+AUTHOR: Austin Minor (米诺)
#+TITLE: Homework 5-1
#+LATEX_HEADER: \usepackage{xeCJK}

* Problems
1. A basket of fruit is being arranged out of apples, bananas, 
   and oranges. What is the smallest number of pieces of fruits that 
   should be put in the basket in order to guarantee that either there
   are at least 8 apples or at least 6 bananas or at least 9 oranges?
2. Show that for any given 52 integers there exists 
   two of them whose sum, or else whose difference, is divisible by 100.
* Solutions
** Problem One
We can think of this as a maximum saturation problem which is a subset
of the pigeon hole theory. We maximize the number of elements we can put
in all positions without violating the constraint. Then, one more element
in any position would violate the constraint.

In our problem, we want to guarantee that we have at least 8 apples, or 6 bananas,
or 9 oranges. The table below shows the maximum value for these elements without satisfying
the constraint. Now, we know that by the pigeon hole principle, picking one more element will
satisfy our constraint as it will increase an already maximized set and fulfill our desired
solution.

| Apple | Bananas | Oranges |
|-------+---------+---------|
|     7 |       5 |       8 |

Thus, the smallest number of pieces of fruit to guarantee the constraint is 1 plus the sum of
the maximums. Thus, $ans = 7 + 5 + 8 + 1 = 21$
** Problem Two
To show that given 52 integers we have a sum or subtraction divisible by 100,
we use the pigeon hole principle and need to first pick a good set of holes.

Our chosen holes are listed below.
(Note: when choosing holes, it is instructive to study
 what is finite in the problem.)

| $i$ | $100-i$ |
|-----+---------|
|   0 |       0 |
|   1 |      99 |
|   2 |      98 |
| ... |     ... |
|  50 |      50 |

These holes cover all the possible remainders of the 52 input numbers divided
by 100. (Note: $100 - 0 = 0 (mod 100)$) We omit the modulo 100 for brevity.

Next, we take a small detour and show that if two numbers have the same remainder
modulo 100 or a remainder $i$ and a remainder $100-i$, we can find a sum or difference
that is divisible by 100.

Case 1 (The two remainders are the same):

If the two remainders are the same, we have $a_1 = 100m_1 + b$ and $a_2 = 100m_2 + b$.
If we subtract these two number we get $a_1 - a_2 = 100m_1 - 100m_2 + b - b = 100(m_1 - m_2)$.
This number is divisible by 100. We are done here.


Case 2 (The two remainders are $i$ and $100-i$):
If the two remainders are $i$ and $100-i$, we have $a_1 = 100m_1 + i$ and $a_2 = 100m_2 + (100-i)$.
If we subtract these two number we get
$a_1 + a_2 = 100m_1 - 100m_2 + i + 100 - i = 100(m_1 - m_2 + 1)$.
This number is divisible by 100. We are done here.

Thus, we have our justification for choosing the above holes. The number of above holes is
51, and we have 52 elements as input. We take the modulo of these 52 elements and
map them to our holes. We are guaranteed (by the pigeon hole principle)
one of the holes will at least
be picked twice. Thus in this hole, either both remainders are the same or they are different
in a solvable way. Since, the holes cover all the remainders, we are guaranteed that no number
can map to a number outside of our holes. Thus by the pigeon hole principle, we can guarantee
the desired outcome of a difference or sum being divisible by 100.
