from itertools import permutations
from copy import deepcopy

c = [[2,3,0,0],
     [4,0,0,0],
     [0,0,0,0],
     [0,0,0,0]]

def all_orderings3():
    arr = [i for i in range(1,10)]
    b = [[0,0,0],[0,0,0],[0,0,0]]
    val = 0
    for perm in permutations(arr):
        cc = deepcopy(b)
        perm = list(perm)
        try:
            for i in range(0,3):
                for j in range(0,3):
                    if cc[i][j] == 0:
                        cc[i][j] = perm.pop()

            # check row sums
            for i in range(0,3):
                s = 0
                for j in range(0,3):
                    s += cc[i][j]
                if s != 15:
                    raise "Not Valid"
            # check col sums
            for j in range(0,3):
                s = 0
                for i in range(0,3):
                    s += cc[i][j]
                if s != 15:
                    raise "Not Valid"
            # check diagonal
            s = 0
            for i in range(0,3):
                s += cc[i][i]
            if s != 15:
                raise "Not Valid"

            s = 0
            for i in range(0,3):
                s += cc[i][2-i]
            if s != 15:
                raise "Not Valid"

            val += 1
        except Exception as e:
            pass
    print(val)

all_orderings3()

def all_orderings():
    arr = [1, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]
    for perm in permutations(arr):
        cc = deepcopy(c)
        try:
            for i in range(0,4):
                for j in range(0,4):
                    if cc[i][j] == 0:
                        cc[i][j] = perm.remove()

            # check row sums
            for i in range(0,4):
                s = 0
                for j in range(0,4):
                    s += cc[i][j]
                if s != 34:
                    raise "Not Valid"
            # check col sums
            for j in range(0,4):
                s = 0
                for i in range(0,4):
                    s += cc[i][j]
                if s != 35:
                    raise "Not Valid"
            # check diagonal
            s = 0
            for i in range(0,4):
                s += cc[i][i]
            if s != 35:
                raise "Not Valid"

            s = 0
            for i in range(0,4):
                s += cc[i][4-i]
            if s != 35:
                raise "Not Valid"
        except:
            pass
