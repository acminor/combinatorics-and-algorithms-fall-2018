#+TITLE: Homework 4.1
#+AUTHOR: Austin Minor (米诺)
#+DATE: \today
#+LATEX_HEADER: \usepackage{xeCJK}
#+LATEX_HEADER: \usepackage{tikz}
#+LATEX_HEADER: \usepackage{pdfpages}

* Questions
1. Find out the number of lattice paths from (0,0) to (n,n+2), 
   which are above but do not cross y=x line? List the formula with n.
2. Find out the number of lattice paths from (0,0) to (n,n+2),
   which are above but do not touch y=x line? List the formula with n.
3. If we want to use positive integers from 1 until 7 to form a ring in order.
   Since 1 and 7 are adjacent to each other in the ring. Due to their neighboring position,
   1 and 7 are also considered as neighbor numbers. Then if we want to pick 3
   non-neighboring numbers from this ring of 7 numbers, how many different solutions are there?
   
* Answers
** Problem One 
#+BEGIN_EXPORT latex
\begin{tikzpicture}
  \draw[->] (0,-1) -- (0,8);
  \draw[->] (-1,1) -- (5,1);
  
  \draw[line width=1mm] (-.25,0) -- (.25,0);
  \draw[line width=1mm] (-.25,2) -- (.25,2);
  \draw[line width=1mm] (-.25,3) -- (.25,3);
  \draw[line width=1mm] (-.25,4) -- (.25,4);
  \draw[line width=1mm] (-.25,5) -- (.25,5);
  \draw[line width=1mm] (-.25,6) -- (.25,6);
  \draw[line width=1mm] (-.25,7) -- (.25,7);
  
  \draw[line width=.2pt] (0,0) -- (5,0);
  \draw[line width=.2pt] (0,2) -- (5,2);
  \draw[line width=.2pt] (0,3) -- (5,3);
  \draw[line width=.2pt] (0,4) -- (5,4);
  \draw[line width=.2pt] (0,5) -- (5,5);
  \draw[line width=.2pt] (0,6) -- (5,6);
  \draw[line width=.2pt] (0,7) -- (5,7);
  
  \draw[line width=1mm] (1,1.25) -- (1,.75);
  \draw[line width=1mm] (2,1.25) -- (2,.75);
  \draw[line width=1mm] (3,1.25) -- (3,.75);
  \draw[line width=1mm] (4,1.25) -- (4,.75);
  
  \draw[line width=.2pt] (1,-1) -- (1,8);
  \draw[line width=.2pt] (2,-1) -- (2,8);
  \draw[line width=.2pt] (3,-1) -- (3,8);
  \draw[line width=.2pt] (4,-1) -- (4,8);
  
  \draw[line width=1mm] (0,0) -- (4,4);
  
  \fill[green] (0,1) circle (.2);
  \fill[red] (.2,2.2) rectangle (-.2,1.8);
  \fill[blue] (.8,0.2) rectangle (1.2,-.2);
\end{tikzpicture}
#+END_EXPORT

The logic is the same as in problem two where it is discussed further.
Except now we note that the mirrored paths
start a different reflection point marked in blue for our invalid paths
starting at the green circle this time. We start at the green circle because
invalid paths start here. 

We set up the problem this way as the line (y=x+1). We use this line as it allows us
to touch the y=x points on a line without crossing over it. If we cross over it we touch
the (y=x+1) line.

Thus, we can apply the same logic
resulting in (n+2) vertical moves and (n) horizontal moves from the green circle.
We also have (n-1) horizontal moves and (n+3) vertical moves from the blue square.

This gives us a final answer of $\binom{2n+2}{n} - \binom{2n+2}{n-1}$
by using logic analogous to the second problem.
** Problem One
#+BEGIN_EXPORT latex
\begin{tikzpicture}
  \draw[->] (0,0) -- (0,8);
  \draw[->] (-1,1) -- (5,1);
  
  \draw[line width=1mm] (-.25,2) -- (.25,2);
  \draw[line width=1mm] (-.25,3) -- (.25,3);
  \draw[line width=1mm] (-.25,4) -- (.25,4);
  \draw[line width=1mm] (-.25,5) -- (.25,5);
  \draw[line width=1mm] (-.25,6) -- (.25,6);
  \draw[line width=1mm] (-.25,7) -- (.25,7);
  
  \draw[line width=.2pt] (0,2) -- (5,2);
  \draw[line width=.2pt] (0,3) -- (5,3);
  \draw[line width=.2pt] (0,4) -- (5,4);
  \draw[line width=.2pt] (0,5) -- (5,5);
  \draw[line width=.2pt] (0,6) -- (5,6);
  \draw[line width=.2pt] (0,7) -- (5,7);
  
  \draw[line width=1mm] (1,1.25) -- (1,.75);
  \draw[line width=1mm] (2,1.25) -- (2,.75);
  \draw[line width=1mm] (3,1.25) -- (3,.75);
  \draw[line width=1mm] (4,1.25) -- (4,.75);
  
  \draw[line width=.2pt] (1,0) -- (1,8);
  \draw[line width=.2pt] (2,0) -- (2,8);
  \draw[line width=.2pt] (3,0) -- (3,8);
  \draw[line width=.2pt] (4,0) -- (4,8);
  
  \draw[line width=1mm] (0,1) -- (4,5);
  
  \fill[green] (0,1) circle (.2);
  \fill[red] (.2,2.2) rectangle (-.2,1.8);
  \fill[blue] (.8,1.2) rectangle (1.2,.8);
\end{tikzpicture}
#+END_EXPORT

Problem one may be seen as in the above diagram (shown is the case
(n=4, n+2=6)). We start at the point
in green, the origin. We must not touch the line y=x

Note that we show a specific case, though we explain our logic in a general case.

We can note that any path that touches y=x has to go through the red square
at (0,1). We also note that this causes a reflected path to be generated starting
at the blue square. Since these reflected paths exist, we can count these paths
to get the number of invalid paths.

The number of the paths from the blue square to the ending point at (n, n+2)
is (n-1) horizontal moves and (n+2) vertical moves. This means we must choose
(n-1) horizontal moves from (n+2 + n-1 = 2n+1) total moves. This equals $\binom{2n+1}{n-1}$.

Next to get the number of valid paths we start with the point above the origin in red.
This point never has to touch the line y=x. From this point we notice we have
(n+2 - 1) vertical moves to choose and (n) horizontal moves to choose. We thus
have (n+1 + n = 2n+1) possible choices to choose (n+1) from. The number of choices
then equals $\binom{2n+1}{n+1}$.

Thus, the total is the total number of paths minus the total number of invalid paths.
This is given by $\binom{2n+1}{n+1} - \binom{2n+1}{n-1}$.
** Problem Three
In problem three, we were unable to find a clever strategy other than noticing
a few observations. Thus, we enumerated the small number of possibilities by hand.
This is shown in our work. From this we notice that their are duplicates among the
valid iterations. Since the numbers are only shifted, these duplicates exist in all
sets. Thus, as shown in our work, we do not calculate these by hand after the first
two sets. Also, any set that contains a number already having all its sets calculated
will be duplicated. This is due to the internal structure of the ring.

With these observations and working this problem out by hand, we determine that the number of
valid sets is 7.

* Work
Note: problem 1 should be labeled problem 2 and vice-versa.
#+BEGIN_EXPORT latex
\includepdf[pages=-]{work-1.pdf}
#+END_EXPORT
