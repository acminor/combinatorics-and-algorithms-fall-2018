from itertools import permutations

def is_valid(perm):
    if (perm[0] == 2 or
        perm[1] == 2 or perm[1] == 3 or
        perm[2] == 3 or perm[2] == 4 or
        perm[3] == 4):
        return False
    return True

input = [1,2,3,4]
print(len([i for i in permutations(input) if is_valid(i)]))
