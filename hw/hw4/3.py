from itertools import combinations_with_replacement

def is_valid(perms):
    len_a = len([c for c in perms if c=='a'])
    len_b = len([c for c in perms if c=='b'])
    len_c = len([c for c in perms if c=='c'])
    len_d = len([c for c in perms if c=='d'])
    if (not (1 <= len_a <= 6) or
        not (0 <= len_b <= 7) or
        not (4 <= len_c <= 8) or
        not (2 <= len_d <= 6)):
        return False
    return True

input = ['a', 'b', 'c', 'd']
print(len([i for i in combinations_with_replacement(input,20) if is_valid(i)]))
