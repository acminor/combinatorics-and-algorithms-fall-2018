#+TITLE: Homework 4.2
#+AUTHOR: Austin Minor (米诺)
#+DATE: \today
#+LATEX_HEADER: \usepackage{xeCJK}
#+LATEX_HEADER: \usepackage{tikz}
#+LATEX_HEADER: \usepackage{pdfpages}

* Problems
1. how many integer numbers from 1 to 10000 are not squares of integers or cubes of integers？
2. How many permutations of 1, 2, 3，……，9 have at least one odd number in its natural position?
3. please calculate the number of integral solutions.
   - $x_1 + x_2 + x_3 + x_4 = 20$
     where $1 \leq x_1 \leq 6, 0 \leq x_2 \leq 7, 4 \leq x_3 \leq 8, 2 \leq x_4 \leq 6$
4. For the permutation P=P1 P2 P3 P4 of {1,2,3,4}, 
   how many feasible permutations are there if we constrain that
   P1≠2，P2≠2、3， P3≠3、4， P4≠4? (4 points)
   
* Solutions
** Problem One
We can start by approaching this as an inclusion/exclusion problem.
We want to know the number of numbers that are not squares or cubes.
We can write this as $|\bar A_{n^2} \cap \bar A_{n^3}|$ where $A_{n^2}$
is numbers in 1 to 10,000 that are integer squares and similarly for $A_{n^3}$.

So this formula is asking for numbers that are not integer squares and not
integer cubes. This can be rewritten using DeMorgan's Law as
$|S| - |A_{n^2} \cup A_{n^3}|$ where $|S|$ is the total set which is the set
of numbers 1 to 10,000. We can now use the inclusion exclusion principle.

#+BEGIN_EXPORT latex
$|\bar A_{n^2} \cap \bar A_{n^3}| = |S| - |A_{n^2} \cup A_{n^3}| = ...$

$... = |S| - (|A_{n^2}| + |A_{n^3}| - |A_{n^2} \cap A_{n^3}|)$
#+END_EXPORT

Next, we determine the largest square and cube number in 10,000 numbers.
This gives us the number of squares and cubes contained within the 10,000 numbers
since our range starts at 1. With this we get $n^2 = 10,000 <=> n=100, n^3 = 10,000 <=> n=21.xxx=21$.
Thus, we now know $|A_{n^2}|$ and $|A_{n^3}|$.

Then comes the difficult task of determining which integer squares are in the set of integer cubes
as well. We were not able to find a general formula. However, since the number of integer cubes
was small, we calculated all the integer cubes and determined if they were integer squares by
taking the square root of them.

|    1 |    2 |    3 |    4 |    5 |    6 |    7 |    8 |    9 |   10 |   11 |
|------+------+------+------+------+------+------+------+------+------+------|
|    1 |    8 |   27 |   64 |  125 |  216 |  343 |  512 |  729 | 1000 | 1331 |
|------+------+------+------+------+------+------+------+------+------+------|

|   12 |   13 |   14 |   15 |   16 |   17 |   18 |   19 |   20 |   21 |      |
|------+------+------+------+------+------+------+------+------+------+------|
| 1728 | 2197 | 2744 | 3375 | 4096 | 3375 | 5832 | 6859 | 8000 | 9261 |      |

From these, we determined that 1,4,9, and 16 cubed were in the range 1 to 10,000 and
also had cubes that were squares. Now, we know $|A_{n^2} \cap A_{n^3}|$.

With this, we can compute our answer. $Ans = 10,000 - (100 + 21 - 4) = 9,883$.

** Problem Two
For this problem, we use the IEP. We establish events
$A_1,A_3,A_5,A_7,A_9$ as the events where 1 is in its natural place, etc.

So we are now looking for all possible cases where 1 is in the natural place,
or three is in the natural place, etc. This is easily modeled as
$A_1 \cup A_3 \cup A_5 \cup A_7 \cup A_9$. This may seem like a lot of
intersections to calculate. However, the structure of the problem makes this easy.

All of the events are independent. Not only that they effect the structure of the problem 
the same. Thus for all terms with only one event chosen, we see that we set one number in its
natural place and have (n-1) numbers to place in there places. The same goes for two events
occurring together, however in this case we have set 2 positions and thus have (n-2) numbers
to place in  their places. This argument can be applied to all combinations of sets. We give an
example of this argument for $A_1$ and $A_3$.

$A_1 => 1 [x x x ... x]$, n-1 x elements to choose in order.
$A_1 => [x x] 3 [x ... x]$, n-1 x elements to choose in order.

With this, we know how many possibilities we have for any generic 
#+BEGIN_EXPORT latex
$A_i \cap A_j \cap ...$
#+END_EXPORT
We also know that to choose two events in our set of 5 events results in $\binom{5}{2}$
number of choices. Since each one of these chooses has the same number of combinations,
we can just multiply the number of ways of choosing k sets times the number of possibilities
for those sets.

Thus, our formulation becomes (with k = number of events and n = number of elements)

$k*(n-1)! - \binom{k}{2}*(n-2)! + \binom{k}{3}*(n-3)! - \binom{k}{4}*(n-4)! + \binom{k}{5}*(n-5)!$

We stop at 5 as k = 5 in our example, and thus all the rest of the terms would be 0.

With this, our answer for n=9 and k=5 (our problem statement) is $Ans = 157,824$.
** Problem Three
We first need to start by doing some substitutions to the equation to give us
a form that is more manageable. First we make all the variables greater than or equal to zero.

Let...
#+BEGIN_EXPORT latex
$y_1 + 1 = x_1$

$y_2 = x_2$

$y_3 + 4 = x_3$

$y_4 + 2 = x_4$
#+END_EXPORT

This gives us some new, more manageable inequalities.

#+BEGIN_EXPORT latex
$0 \leq y_1 \leq 5$

$0 \leq y_2 \leq 7$

$0 \leq y_3 \leq 4$

$0 \leq y_4 \leq 4$
#+END_EXPORT

Furthermore, this changes our original equation.

$y_1 + 1 + y_2 + y_3 + 4 + y_4 + 2 = 20$

$y_1 + y_2 + y_3 + y_4 = 13$

Next, we use DeMorgan's rule and the IEP to rewrite our initial equation.
$|A_1 \cap A_2 \cap A_3 \cap A_4| = TotalPossible - |\bar A_1 \cup \bar A_2 \cup \bar A_3 \cup \bar A_4|$
Where $A_i$ is the new inequality formed with $y_i$ and $\bar A_i$ is the negation of that
inequality. These negations are listed below for reference.

#+BEGIN_EXPORT latex
$\bar y_1 \geq 6$

$\bar y_2 \geq 8$

$\bar y_3 \geq 5$

$\bar y_4 \geq 5$
#+END_EXPORT

Now, we turn our attention to analyzing the IEP union term.
#+BEGIN_EXPORT latex
$|\bar A_1 \cup \bar A_2 \cup \bar A_3 \cup \bar A_4| = ...$

$...=|\bar A_1| + |\bar A_2| + |\bar A_3| + |\bar A_4| - ...$

$... |\bar A_1 \cap \bar A_2| - |\bar A_1 \cap \bar A_3| - |\bar A_1 \cap \bar A_4| -
|\bar A_2 \cap \bar A_3| - |\bar A_2 \cap \bar A_4| - |\bar A_3 \cap \bar A_4| + ...$

$... |\bar A_1 \cap \bar A_2 \cap \bar A_3| + |\bar A_1 \cap \bar A_2 \cap \bar A_4| +
|\bar A_1 \cap \bar A_3 \cap \bar A_4| + |\bar A_2 \cap \bar A_3 \cap \bar A_4| - ...$

$... |\bar A_1 \cap \bar A_2 \cap \bar A_3 \cap \bar A_3|$
#+END_EXPORT

What follows is a table of these values. The work is in the work section.

#+BEGIN_EXPORT latex
\begin{tabular}{|l|l|}
\hline
  $|\bar A_1|$ & $\binom{7+3}{3}$\\
  $|\bar A_2|$ & $\binom{5+3}{3}$\\
  $|\bar A_3|$ & $\binom{8+3}{3}$\\
  $|\bar A_4|$ & $\binom{8+3}{3}$\\
  $|\bar A_1 \cap \bar A_2|$ & $0$\\
  $|\bar A_1 \cap \bar A_3|$ & $\binom{2+3}{3}$\\
  $|\bar A_1 \cap \bar A_4|$ & $\binom{2+3}{3}$\\
  $|\bar A_2 \cap \bar A_3|$ & $1$\\
  $|\bar A_2 \cap \bar A_4|$ & $1$\\
  $|\bar A_3 \cap \bar A_4|$ & $\binom{3+3}{3}$\\
  $|\bar A_1 \cap \bar A_2 \cap \bar A_3|$ & $0$\\
  $|\bar A_1 \cap \bar A_2 \cap \bar A_4|$ & $0$\\
  $|\bar A_1 \cap \bar A_3 \cap \bar A_4|$ & $0$\\
  $|\bar A_2 \cap \bar A_3 \cap \bar A_4|$ & $0$\\
  $|\bar A_1 \cap \bar A_2 \cap \bar A_3 \bar A_4|$ & $0$\\
\hline
\end{tabular}
#+END_EXPORT

Knowing this and know that the solution to this problem without constraints
is $\binom{13+3}{3}$ (i.e. the problem of choosing 3 bars to separate a set into
4 groups without caring about the ordering of those bars). 

With that and solving our previous equations,
$Ans = 96$

** Problem Four
In problem four, we use the IEP and DeMorgan's Law to calculate the answer.
Since we are given constraints that we cannot have numbers in a certain position,
we consider all the ways of getting those numbers in those positions and subtract that from
the total number of positions. This is done in our work.

To further explain this, for each set we consider given our choices how many free choices are we given
in addition to these. These are our multiplicative choices. We also consider how many choices we have
in our forced choices. These are our categorical or additive choices.

Categorical choices come from the $P_i$ numbers and multiplicative come from the remaining
numbers to choose. In the work, we used this to get our terms.

We then solve for the terms in the equation $|Total| - |A_{P1} \cup ... \cup A_{P2}|$
where $A_{Pi}$ is the set where all $P_i$ constraints we do not satisfy them.
We also note that once we have used a categorical number we cannot use it again in that
same category of calculation.

We note that the Total is merely the number of ways of permutating 4 elements.

With this we get $Ans = 4$.

(Check the work for further explanation.)

* Work
#+BEGIN_EXPORT latex
\includepdf[pages=-]{work-2.pdf}
#+END_EXPORT
