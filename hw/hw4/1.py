from math import *

def is_square(val):
    for i in range(0, val+1):
        if i*i == val:
            return True
    return False

def is_cube(val):
    for i in range(0, val+1):
        if i*i*i == val:
            return True
    return False

print(len([i for i in range(1,10001) if not is_square(i) and not is_cube(i)]))
