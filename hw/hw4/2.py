from itertools import permutations

def is_odd(num):
    if num/2.0 != num/2:
        return False
    return True

def odd_right(perm):
    for i in range(0, len(perm)):
        if i+1 == perm[i] and is_odd(i):
            return True
    return False

input = [i for i in range(1, 10)]
print(len([i for i in permutations(input) if odd_right(i)]))
