from copy import deepcopy

class GraphNode:
    def __init__(self):
        # technically allows parallel edges (assumes graph construction is proper)
        self._edges = []
    def add_edge(self, node):
        self._edges.append(node)
    def get_edges(self):
        return self._edges
    def __repr__(self):
        return str(self._pos)

# class Graph:
#     def __init__(self):
#         # use array for benefits later
#         self._nodes = []
#     def add_node(self, node):
#         self._nodes.append(node)
#     def get_nodes(self):
#         return self._nodes
#     def _set_nodes(self, nodes):
#         self._nodes = nodes
#     def get_num_nodes(self):
#         return len(self._nodes)

def create_lattice_path(m,n):
    # because of structure
    m = m + 1
    n = n + 1
    # n column tall grid
    # with m width rows
    grid = [[GraphNode() for unused in range(0,m)] for i in range(0,n)]

    for j in range(0,n):
        for i in range(0,m):
            if j != n - 1:
                # vertical edge
                grid[-1 - j][i].add_edge(grid[-1-j-1][i])
            if i != m - 1:
                # horizontal edge
                grid[-1 - j][i].add_edge(grid[-1-j][i+1])
            grid[-1 - j][i]._pos = (i,j)

    return grid[-1][0]

def enumerate_paths(startNode):
    class Stats:
        def __init__(self):
            self.clr()

        def clr(self):
            self.path = []
            self.paths = []

    this = enumerate_paths
    this.stats = Stats()

    CUR_VISIT_COLOR = 10

    def _recursive_dfs(node):
        this.stats.path.append(node._pos)

        if len(node.get_edges()) == 0:
            this.stats.paths.append(deepcopy(this.stats.path))

        for connected_node in node.get_edges():
            _recursive_dfs(connected_node)

        this.stats.path.pop()

    this.stats.clr()
    _recursive_dfs(startNode)
    return this.stats

# assume slope = 1
def touches_line(point, b):
    x = point[0]
    y = point[1]
    if y - x == b:
        return True
    return False

startNode = create_lattice_path(4,6)
stats = enumerate_paths(startNode)
# exclude start point
print(len([i for i in stats.paths if not any([touches_line(point, 0) for point in i[1:]])]))
print(len([i for i in stats.paths if not any([touches_line(point, -1) for point in i[1:]])]))
