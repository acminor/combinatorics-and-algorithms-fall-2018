from itertools import combinations

neighbors = dict()
for i in range(0,7):
    neighbors[i] = set()
    left = (i - 1) % 7
    right = (i + 1) % 7
    neighbors[i].add(left)
    neighbors[i].add(right)
print(neighbors)

input = range(0,7)
combos = combinations(input, 3)
#print([[j+1 for j in i] for i in combos if not any([nb in neighbors[i[0]] for nb in i[1:]])
                                           #or any([nb in neighbors[i[1]] for nb in (i[0], i[2])]))

v_combos = list()
for i in combos:
    c_set1 = neighbors[i[0]]
    c_set2 = neighbors[i[1]]
    set1 = [i[1],i[2]]
    set2 = [i[0],i[2]]
    c_1 = any([nb in c_set1 for nb in set1])
    c_2 = any([nb in c_set2 for nb in set2])
    if not c_1 and not c_2:
        v_combos.append(i)

print(len(v_combos))
