import random

def random_select(arr, elem):
    def rI():
        return random.randint(0, len(arr) - 1)

    count = 0
    indices = [i for i in range(0, len(arr))]
    while(len(indices) != 0):
        ri = rI()
        count += 1
        try:
            indices.remove(ri)
        except:
            pass

        if arr[ri] == elem:
            return ri, count
    return None, count

trials = 1000
print("fmt: arr, index, count")
print()

print("1-elem")
a = [i for i in range(0, 10)]
def t():
    l = 0
    for i in range(0, trials):
        _, ll = random_select(a, 1)
        l += ll
    print(l/float(trials))
t()

print("K-elem")
def k_elem(k, n):
    a = [1 for i in range(0, k)] + [2 for i in range(0, n-k)]
    return a

print("k=2")
a = k_elem(2, 10)
def t1():
    l = 0
    for i in range(0, trials):
        _, ll = random_select(a, 1)
        l += ll
    print(l/float(trials))
t1()

print("k=5")
a = k_elem(5, 10)
def t2():
    l = 0
    for i in range(0, trials):
        _, ll = random_select(a, 1)
        l += ll
    print(l/float(trials))
t2()

print("None")
a = [i for i in range(0, 10)]
def t3():
    l = 0
    for i in range(0, trials):
        _, ll = random_select(a, 10)
        l += ll
    print(l/float(trials))
t3()
