import itertools

a = [i for i in range(0, 8)]

count = 0
leng = 0
for p in itertools.permutations(a):
    leng += 1
    for i, iv in enumerate(p):
        for j, jv in enumerate(p):
            if i < j and iv > jv:
                count += 1
print(count/float(leng))
