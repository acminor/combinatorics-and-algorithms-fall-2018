import random

def die_roll(k=6):
    return random.randint(1,k)

def trial(n, die_roll):
    sum = 0
    for i in range(0, n):
        sum += die_roll()
    return sum

def trials(num_trials, trial, die_roll=die_roll):
    sum = 0.0
    for i in range(0, num_trials):
        sum += trial(die_roll)
    avg = sum / float(num_trials)
    return avg

n=20
print(trials(100000, lambda die_roll: trial(n, die_roll), die_roll))
