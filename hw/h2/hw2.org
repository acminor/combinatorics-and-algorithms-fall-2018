#+TITLE: Homework 2.1
#+AUTHOR: Austin Minor (米诺)
#+DATE: \today
#+LATEX_HEADER: \usepackage{xeCJK}
#+LATEX_CLASS: report

* Questions
1) How many different permutations for word “Combinatorics”? (Case sensitive)
2) The coefficient number of $a^2b^2c^2$ in the 
   expanded equation of $(2a+b+c)^6$ is ____。【Please calculate the exact number】
3) For the case of giving fruits to 3 kids, 
   in total there are 12 identical apples, each child may* (must?)
   at least have one apple, how many different ways to give apples to 3 kids?
4) What is the number of integral solutions of the 
   equation $x_1+x_2+x_3=30$, in which $x_1\geq5$, $x_2\geq-8$, $x_3\geq5$.
* Answers
** Problem One
|--------+-------|
| Letter | Count |
|--------+-------|
| C      |     1 |
| o      |     2 |
| m      |     1 |
| b      |     1 |
| i      |     2 |
| n      |     1 |
| a      |     1 |
| t      |     1 |
| r      |     1 |
| c      |     1 |
| s      |     1 |
|--------+-------|
|        |    13 |
|--------+-------|

Divide the total number of permutations by the number of repeated letter permutations.
In our case, it is o and i. This will generate the number of possible non-repeated
permutations.

$ans = \frac{P(13,13)}{P(2,2)*P(2,2)} = \frac{13!}{2!*2!} = 1556755200$

Note: C and c are different in a case sensitive system.

** Problem Two

For ease of calculation, we first start by replacing the some of the variables.
Let $x = 2a, y=b, z=c, r=(y+z)$.

With this we have, $(x+r)^6$. This is an equation we can expand. However, we are
only interested in the coefficient number for $a^2b^2c^2$. Thus, we start with finding the
term for $x^2$ which would be $(2a)^2$.

#+BEGIN_EXPORT latex
$(x+r)^6 = ... + C(6,4) x^2 r^4 + ...$

Next, we determine when $r^4$ gives us $y^2 z^2$ corresponding to $b^2 c^2$.

$r^4 = (y + z)^4 = ... + C(4,2) y^2 z^2 + ...$

This gives us:

$(x+r)^6 = ... + C(6,4) x^2 (... + C(4,2) y^2 z^2 + ...) + ...$

Distributing results in:

$(x+r)^6 = ... + (... + C(6,4) x^2 C(4,2) y^2 z^2 + ...) + ...$

$(x+r)^6 = ... + (... + C(6,4) C(4,2) x^2 y^2 z^2 + ...) + ...$

Substituting results in:

$(x+r)^6 = ... + (... + C(6,4) C(4,2) (2a)^2 b^2 c^2 + ...) + ...$

$(x+r)^6 = ... + (... + C(6,4) C(4,2) 4 a^2 b^2 c^2 + ...) + ...$

Resulting in a coefficient for $a^2b^2c^2$ of:

$ans = C(6,4) C(4,2) 4 a^2 b^2 c^2 = 360 a^2b^2c^2$
#+END_EXPORT

** Problem Three
In the case of each kid having 1 or more apples, we can see this as a case of
partitioning. How can we partition all the possible apples among three kids. We can
use two bars to partition the apples into three groups. Group 1 - kid 1, etc.
Since each kid must have one apple, we do not have to deal with a kid getting none.
Thus, before and after each apple is a blocker position (some overlapping).
This is except for the start and end of the apples as each kid must have at least 1 apple.

x0x0x0x0x0x0x0x0x0x0x0x

x = apple

0 = potential bar position

Thus, we are choosing 2 positions regardless of order from 11. Thus,
we have, C(11,2).

$ans = C(11,2) = 55$

** Problem Four
We start by creating some new variables for the equation.
Let $y_1 = x_1 - 5, y_2 = x_2 + 8, y_3 = x_3 - 5$.

Substituting this into our equation yields:

$(y_1 + 5) + (y_2 - 8) + (y_3 + 5) = 30$

$y_1 + y_2 + y_3 = 30 - 5 + 8 - 5$

$y_1 + y_2 + y_3 = 28$

This is now a problem of permutations and bars.
We have redefined our problem where each variable may have
0 or more elements. Thus, we must divide 28 elements
into three sets. We can do this with 2 bars. Thus, we need
to permutate the two bars and 28 elements because the two bars
could be adjacent (0 or more elements), etc. We then divide
out the identical permutations of the bars and of the elements.

$ans = \frac{P(28+2)}{P(2,2)P(28,28)} = 435$
